# Install HELM
```shell
brew install helm
```


# Intall Helm-Push Plugin

```shell
helm plugin install https://github.com/chartmuseum/helm-push.git
```

# Deploy project

## Prerequisites
* Add Private Docker Registry to k8s cluster
* Get kube.config

## 
* `${registry}` and `$build-tag` come from Build Docker step
* `${helm-page}` is supported among this repo: `go-base | release-notes`

```shell
helm upgrade --install ${DEPLOY_APP} go-base/ --set image.repository=$IMAGE --set image.tag=$TAG --set fullnameOverride=${DEPLOY_APP} --kubeconfig config/kube.config --namespace ${K8S_PROD_NAMESPACE}
```

# Install custom helm repo

```shell
helm repo add chartmuseum http://chartmuseum.host:8080 # add our custom http helm repo with name chartmuseum

helm repo update #reload all helm chart from new repo
```

# Install application


```shell
helm install my-app chartmuseum/go-base # repo-name/app-name
```

Upgrade new version

```shell

helm upgrade -f project_values.yaml my-app chartmuseum/go-base
```
